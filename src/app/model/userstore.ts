export class Userstore {
    access_token?:string;
    token_type?:string;
    expires_in?:string;
    refresh_token?:string;
    client_id?:string;
    username?:string;
    issued?:string;
    expires?:string;
}
