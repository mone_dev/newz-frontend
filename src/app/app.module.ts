import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EffectsModule } from '@ngrx/effects';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { DownloadComponent } from './download/download.component';
import { PricingComponent } from './pricing/pricing.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RefillComponent } from './refill/refill.component';
import { LogoutComponent } from './logout/logout.component';
import { AuthService } from './store/services/auth.service';
import { AuthGuardService } from './store/services/auth.guard';
import { AuthEffects } from './store/effects/auth.effects';
import { reducers } from './store/app.states';
import { TokenInterceptor, ErrorInterceptor } from './store/services/token.interceptor';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DownloadComponent,
    PricingComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    RefillComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    StoreModule.forRoot(reducers, {}),

    EffectsModule.forRoot([AuthEffects]),
    NgbModule.forRoot(),
    RouterModule.forRoot([

      { path: 'Home', component: HomeComponent },
      { path: 'Home/Download', component: DownloadComponent },
      { path: 'Home/Pricing', component: PricingComponent },
      { path: 'Home/Login', component: LoginComponent },
      { path: 'Home/Register', component: RegisterComponent },

      { path: 'User/Dashboard', component: DashboardComponent,canActivate: [AuthGuardService] },
      { path: 'User/Refill', component: RefillComponent,canActivate: [AuthGuardService] },
      { path: 'User/Logout', component: LogoutComponent,canActivate: [AuthGuardService] },


      { path: '', component: HomeComponent },
      { path: '**', redirectTo: '/' }
    ])


  ],
  providers: [

    AuthService,
    AuthGuardService,

    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    }

  ],
  bootstrap: [AppComponent]

})

export class AppModule { }
