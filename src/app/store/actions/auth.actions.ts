import { Action } from '@ngrx/store';


export enum AuthActionTypes {
  LOGIN = '[Auth] Login',
  LOGIN_SUCCESS = '[Auth] Login Success',
  LOGIN_FAILURE = '[Auth] Login Failure',
  GET_STATUS = '[Auth] GetStatus',
  GET_STATUS_TRUE = '[Auth] GetStatusTrue',
  GET_STATUS_FALSE = '[Auth] GetStatusFALSE'
}

export class LogIn implements Action {
  readonly type = AuthActionTypes.LOGIN;
  constructor(public payload: any) {}
}

export class LogInSuccess implements Action {
  readonly type = AuthActionTypes.LOGIN_SUCCESS;
  constructor(public payload: any) {}
}

export class LogInFailure implements Action {
  readonly type = AuthActionTypes.LOGIN_FAILURE;
  constructor(public payload: any) {}
}

export class GetStatus implements Action {
  readonly type = AuthActionTypes.GET_STATUS;
  constructor(public payload: any) {}
}

export class GetStatusTRUE implements Action {
  readonly type = AuthActionTypes.GET_STATUS_TRUE;
  constructor(public payload: any) {}
}

export class GetStatusFALSE implements Action {
  readonly type = AuthActionTypes.GET_STATUS_FALSE;
  constructor(public payload: any) {}
}



export type All =
  | LogIn
  | LogInSuccess
  | LogInFailure
  | GetStatus
  | GetStatusTRUE
  | GetStatusFALSE;