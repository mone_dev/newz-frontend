import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import { tap } from 'rxjs/operators';

import {
  AuthActionTypes,
  LogIn, LogInSuccess, LogInFailure, GetStatus, GetStatusTRUE, GetStatusFALSE
} from '../actions/auth.actions';

import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthEffects {

  constructor(
    private actions: Actions,
    private authService: AuthService,
    private router: Router,
  ) { }

  // effects go here
  @Effect()
  LogIn: Observable<any> = this.actions
    .ofType(AuthActionTypes.LOGIN)
    .map((action: LogIn) => action.payload)
    .switchMap(payload => {
      return this.authService.logIn(payload.email, payload.password)
        .map((user) => {
          
          return new LogInSuccess(user);
        })
        .catch((error) => {
          console.log(error);
          return Observable.of(new LogInFailure(error.error));
        });
    });

  @Effect({ dispatch: false })
  LogInSuccess: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.LOGIN_SUCCESS),
    tap((user) => {
      localStorage.setItem('token', JSON.stringify(user.payload));
      this.router.navigateByUrl('/User/Dashboard');
    })
  );

  @Effect({ dispatch: false })
  LogInFailure: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.LOGIN_FAILURE)
  );

  @Effect()
  GetStatus: Observable<any> = this.actions
    .ofType(AuthActionTypes.GET_STATUS)
    .map((action: GetStatus) => action.payload)
    .switchMap(payload => {
      return this.authService.getStatus()
        .map((user) => {
          localStorage.setItem('activate', 'true');         
          return new GetStatusTRUE(true);
        })
        .catch((error) => {
          localStorage.setItem('activate', 'false');
          return Observable.of(new GetStatusFALSE(false));
        });
    });

}
