import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { AuthService } from './auth.service';
import { AppState, selectAuthState  } from '../../store/app.states';
import { Store } from '@ngrx/store';
import { GetStatus } from '../actions/auth.actions';



import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(
    public auth: AuthService,
    public router: Router,
    private store: Store<AppState>
  ) { this.getState = this.store.select(selectAuthState);}
  activate:boolean =false;

  getState: Observable<any>;


  isAuthenticated: boolean | null;



  canActivate(): boolean {
    this.store.dispatch(new GetStatus(''));
    if(this.router.url==="/Home/Login")
    return true;
    
    this.getState.subscribe((state) => {

      this.isAuthenticated = state.userHandle;
      if (this.isAuthenticated == null) {
        if (localStorage.getItem('activate') == 'true')
          this.isAuthenticated = true;
        else
          this.isAuthenticated = false;
      }

    
    });
    console.log("hhh"+this.isAuthenticated);
    return this.isAuthenticated;
  }


}