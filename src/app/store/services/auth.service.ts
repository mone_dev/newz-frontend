import { HttpClient} from '@angular/common/http';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  private BASE_URL = 'http://localhost:53290/';

  constructor(private http: HttpClient) { }


  getToken() {
    return localStorage.getItem('token');
  }

  logIn(email: string, password: string): Observable<any> {
    const url = this.BASE_URL+'token';
    return this.http.post(url, "grant_type=password&username="+email+"&password="+password+"&client_id=ngAuthApp");
  }

  getStatus(): Observable<any> {
    const url = this.BASE_URL+'Api/Authentication/checklogin';
    return this.http.get(url);
  }

}
