import { Injectable, Injector } from '@angular/core';

import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse
} from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

import 'rxjs/add/operator/do';

import { Router } from '@angular/router';

import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptor implements HttpInterceptor {

  private authService: AuthService;
  constructor(private injector: Injector) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.authService = this.injector.get(AuthService);
    var token = this.authService.getToken();

      //checklink
      console.log(request);
      if (request.url.indexOf('/token') < 1 && token!=null) {
        console.log('Bearer');
        request = request.clone({
          setHeaders: {
            'Authorization': 'Bearer ' + JSON.parse(token)['access_token'],
            'Content-Type': 'application/json'
          }
        });
      }

    return next.handle(request);

  }
}

@Injectable({
  providedIn: 'root'
})
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private router: Router) { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request)
      .catch((response: any) => {
        if (response instanceof HttpErrorResponse && response.status === 401) {
          localStorage.clear();
          return ErrorObservable.create(false);
        }
        return Observable.throw(response);
      });
  }
}

