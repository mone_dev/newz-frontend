import { AuthActionTypes, All } from '../actions/auth.actions';

import { User } from '../../model/user';
export interface State {
    isAuthenticated: boolean;
    user: User | null;
    errorMessage: string | null;
    userHandle:boolean|null;
  }

  export const initialState: State = {
    isAuthenticated: false,
    user: null,
    errorMessage: null,
    userHandle:null
  };

  export function reducer(state = initialState, action: All): State {
    console.log(action);
    switch (action.type) {
      case AuthActionTypes.LOGIN_SUCCESS: {
        return {
          ...state,
          isAuthenticated: true,
          user: {
            email: action.payload.email,
          },
          errorMessage: null
        };
      }

      case AuthActionTypes.LOGIN_FAILURE: {
        return {
          ...state,
          errorMessage: 'Incorrect email and/or password.'
        };
      }

      case AuthActionTypes.GET_STATUS_TRUE:{
        return {...state,userHandle:true};
      }

      case AuthActionTypes.GET_STATUS_FALSE:{
        return {...state,userHandle:false};
      }

      default: {
        return state;
      }
    }
  }

