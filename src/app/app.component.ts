import { Component } from '@angular/core';
import { Userinfo } from './viewmodel/userinfo';
import { Observable } from 'rxjs/Observable';
import { AppState, selectAuthState } from './store/app.states';
import { Store } from '@ngrx/store';
import { GetStatus } from './store/actions/auth.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'NewZ-Frontend';
  getState: Observable<any>;
  userinfo: Userinfo;

  isAuthenticated: boolean | null;

  constructor(private store: Store<AppState>) {

    this.getState = this.store.select(selectAuthState);
  }

  ngOnInit() {
    

    this.getState.subscribe((state) => {
      
      this.isAuthenticated = state.userHandle;
      if (this.isAuthenticated == null) {
        if (localStorage.getItem('activate') == 'true')
          this.isAuthenticated = true;
        else
          this.isAuthenticated = false;
      }

      console.log(this.isAuthenticated);
    });




  }
}
